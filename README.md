# TP2 - Organización del Computador II #

## Estructura de Directorios ##

La organización de las carpetas del Trabajo Práctico es la siguiente

* tests/  :                           Contiene todas las imagenes con los que se prueban los métodos
* img/  :                           Contiene todas las imagenes de la cátedra
* informe/  :                        Contiene el PDF del informe y su Latex
* plots/  :                          Contiene todos los scripts de Gnuplot para realizar los gráficos
 y un Makefile para la generación de los datos necesarios (cuando hagan falta) para los scripts 
 de ploteo
* scripts
	* scripts/medir_tiempo_tamano_variable_blur.sh:      Imprime la medición de tiempos del calculo del blur de una imagen para una determinado implementación y un determinado rango de tamaños
	* scripts/medir_tiempo_tamano_variable_diff.sh:      Imprime la medición de tiempos del calculo del diff de una imagen para una determinado implementación y un determinado rango de tamaños
	* scripts/medir_tiempo_radio_variable_blur.sh:      Imprime la medición de tiempos del calculo del blur de una imagen para una determinado implementación y un determinado rango de radios
	* scripts/stats.py				 Obtiene el promedio y el desvio estandar de una muestra de tiempos
* filtros/  :                            Aqui se encuentra todo el código de los filtros

## Como correr el TP ##

Para la construcción de los archivos de experimentación usados en el TP, como la construcción del informe o los objetos y binarios a partir del código fuente utilizamos archivos Makefiles junto con la herramienta **make** que van compilando todo a medida que es necesario sin necesidad de recompilar todo cada vez.
	    
Cada carpeta salvo tests/, scripts/, img/ y build/ tienen sus Makefiles para la construcción de los archivos antes mencionados. Además hay un Makefile en la carpeta principal que recursivamente va haciendo make en cada carpeta que posea el archivo Makefile. 

Resumiendo, para compilar: utilizar el comando **make** en la carpeta donde se desee construir los objetos o en la carpeta principal para construir todo y **make clean** para borrar todos los objetos compilados.


## Herramienta para los gráficos ##

Como se dijo combinamos la herramienta **make** para construir los datos necesarios (la medicion de tiempos) y junto con el programa **gnuplot** realizamos los graficos usando los scripts en formato gnuplot leyendo los datos proveidos por la herramienta **make**.

Los scripts de gnuplot se encuentran en la carpeta plots/ y tienen extensión .gpi

