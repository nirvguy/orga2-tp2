\section{Desarrollo}

	Para cada filtro realizamos dos implementaciones, una en C y una en ASM. En
	la implementación de C no utilizaremos explícitamente el modelo de
	procesamiento SIMD (Single Instruction Multiple Data) ni ningún otro modelo
	que utilice procesamiento de datos en paralelo, en cambio en ASM
	aprovecharemos, como se explicará mas adelante, este modelo para procesar
	en paralelo varios datos.

	Para las implementaciones que utilicen SIMD aprovecharemos que la
	especificación nos asegura que los tamaños de imágenes serán múltiplos de
	4.

\subsection{Diff}

\subsubsection{Diff en C}

	Viendo la imagen como una matriz de píxeles, recorremos cada fila y columna
	de ambas imágenes de entrada a la vez, calculando la diferencia en valor
	absoluto de cada componente y quedándonos con el máximo para luego
	replicarlo en las 3 componentes del pixel que irá a la imagen de salida.
	Finalmente fijamos la componente $a$ en 255 para cumplir con la
	especificación dada.


\subsubsection{Diff en ASM}
	
	Para esta implementación vemos a la imagen como un vector de píxeles, no
	como matriz. Como cada pixel tiene 4 componentes, compuestas por un byte
	cada una (0-255), observamos que en un registro XMM, de tamaño 128 bits,
	podremos almacenar exactamente 4 píxeles (16 bytes) empaquetados como
	bytes. 
	 
	Recorremos el vector procesando 4 píxeles en cada iteración, en el registro
	$xmm0$ y $xmm2$ cargaremos la información de una de las dos imágenes y en
	$xmm1$ la información correspondiente de la otra.

	Realizamos la carga por duplicado de los datos de una imagen porque
	haremos una comparación de máximos y mínimos entre los datos de ambas
	entradas, quedando en $xmm0$ los valores máximos entre
	$xmm0$ y $xmm1$ y en $xmm2$ los valores mínimos entre $xmm2$ y $xmm1$,
	todos vistos como bytes. Luego el calculo de diferencia absoluta
	se realizara restándole $xmm2$ a $xmm0$.

	Una vez obtenida la diferencia absoluta de cada componente de cada píxel
	debemos quedarnos con el máximo valor para cada píxel para luego replicarlo
	en las otras componentes.
	Esto lo realizaremos copiando en los registros $xmm1$ y $xmm2$ los valores
	calculados previamente y guardados en $xmm0$. 
	Luego a $xmm1$ le aplicaremos un shift lógico hacia la izquierda de 8
	posiciones y a $xmm2$ uno de 16 posiciones, de esta forma al realizar un
	cálculo de máximos entre $xmm0$ y $xmm1$ obtendremos en $xmm0$ el máximo de
	diferencias entre las componentes $r$ y $g$, y al hacer lo mismo entre
	$xmm0$ y $xmm2$ obtendremos el máximo entre las 3 componentes $r,g,b$ cada
	3 bytes del registro $xmm0$.

	Luego utilizando una máscara junto a la instrucción $pshufb$ replicaremos
	estas posiciones relativas para cada píxel.

	Los movimientos de datos y de cálculos se realizarán
	aprovechando las instrucciones SIMD provistas por $Intel$
	logrando procesar la diferencia absoluta de todas las componentes de 4
	píxeles contiguos en paralelo.

	En la siguiente pagina se ejemplifica
	todo este proceso.

	\includepdf[pagecommand={},scale=0.9,pages=-]{./imagenes/Diagrama_Diff_ASM.pdf}

	
	\subsection{Desenfoque Gaussiano}


\subsubsection{Desenfoque en C}

	Para la implementación de esta función en C, primero generamos la matriz de
	convolución en base al sigma y radio pasados como parámetro.
	
	Empezamos recorriendo la imagen desde el píxel ubicado en la fila y
	columna igual al radio, ya que como fue mencionado previamente dejaremos un
	borde sin procesar. En estas iteraciones avanzaremos de a 1 píxel, y nos
	detendremos al encontrar nuevamente el borde al otro lado de la imagen.

	Una vez situado en un pixel, realizaremos una iteración sobre la matriz de
	convolución, multiplicando a cada píxel vecino (componente por componente)
	con su coeficiente correspondiente en dicha matriz, convirtiendo los
	datos a floats previamente para obtener la precisión requerida. 
	
	Tendremos para cada una de las 3 componentes del píxel evaluado, una
	variable que irá acumulando la sumatoria de las multiplicaciones
	realizadas. Este dato, al finalizar de recorrer toda la matriz, tendrá el
	promedio ponderado que se utilizará en la imagen de salida.

	Finalmente se pasan estos datos nuevamente a byte y, luego de asegurarnos
	que la componente $a$ sea igual a 255, guardamos la información del píxel en
	la imagen destino.


\subsubsection{Desenfoque en ASM}

	En la implementación en ASM primero llamaremos a una función en C para
	generar la matriz de convolución, una vez obtenida esta matriz recorreremos de manera
	similar a como se recorre en la implementación en C pero con la ventaja de
	que en los registros $XMM$ podremos procesar al mismo tiempo las 4
	componentes de un píxel en paralelo utilizando el modelo SIMD.

	Como la imagen la vemos como un vector y no como una matriz, tuvimos que
	guardar en el stack frame dos variables y realizar una función que revise
	si el píxel que se procesa es parte del borde que se deja sin modificar.
	
	A diferencia del filtro DIFF, el Desenfoque Gaussiano necesita tratar con
	números de punto flotante de 32 bits (norma IEE754) para conseguir el
	resultado deseado. Es por este motivo que podremos procesar un solo píxel
	por vez ya que, como se hizo en la implementación en C, tendremos que
	convertir las 4 componentes a punto flotante. Esta conversión también
	podremos hacerla en paralelo con una instrucción del modelo SIMD.

	Una vez que tenemos en consideración estos dos puntos podremos comenzar a
	correr la imagen. Encontraremos un píxel a procesar, cargaremos la
	información en el registro $xmm1$, con un $pshufb$ ordenaremos los datos para
	convertir los valores de las componentes de un píxel de $byte$ a $double
	word$ y luego a representación de punto flotante con $cvtq2ps$.

	Luego traeremos el valor de la celda de la matriz correspondiente a la
	misma posición en la imagen centrada en el pixel a evaluar y repetiremos
	ese $float$ en el registro $xmm2$. Multiplicaremos en paralelo los valores
	de $xmm1$ con los de $xmm2$ y con esto quedara cada componente multiplicada
	por el valor correspondiente de la celda de la matriz $K$, ese resultado se
	acumulará en $xmm0$.

	Una vez realizado estos cálculos para todos los valores de la matriz $K$,
	convertiremos los 4 valores resultantes en enteros de 32 bits (con
	$cvtps2dq$) para luego pasarlos a enteros de 1 byte con otro $pshufb$ y
	luego almacenarlos en la imagen de salida. 

	Se asume que el resultado de los cálculos queda entre 0 y 255 ya que es un
	promedio con coeficientes todos menores a 1 de valores entre 0 y 255.
	
	En la siguiente pagina se ejemplifica
	todo este proceso.

	\includepdf[pagecommand={},pages=-,scale=0.9]{./imagenes/Diagrama_Blur_ASM.pdf}
