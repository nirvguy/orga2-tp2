\section{Experimentación}

	Como vimos en la implementación de ambos filtros, el código en ASM puede
	aprovechar el modelo SIMD para procesar paquetes de datos en forma
	paralela. Es por esto que esperamos que la implementación en ASM tenga una
	mejor Performance en relación con la implementación en C. 
	
	En nuestros experimentos vamos a variar el tamaño de ancho y alto de la
	imagen a la vez y siempre en múltiplos de 4, respetando la especificación.
	Consideramos esto aprovechando que como ASM lee la imagen como un
	vector, le es indistinto la relación entre tamaños, y en C los jumps entre
	ciclos son despreciables en comparación al procesamiento dentro del mismo.

	Cada medición de tiempo es calculada 20 veces, y promediamos los
	resultados (medidos en ticks de clock, ya que nos pareció lo mas fino para
	medir tiempos) que junto al cálculo de la desviación estándar obtendremos
	un resultado representativo y que disminuya la aparición de $outliers$. 
	Todos estos experimentos fueron medidos en una misma maquina Intel(R) Core(TM) i5 760 @ 2.80GHz
	para poder tener una buena uniformidad de la muestra.

	Esperamos que, para ambos filtros, las primeras mediciones en donde las
	imágenes son pequeñas difieran de las mediciones promedio, ya que dependiendo 
	del tamaño de la memoria cache y la administración de la misma por parte del
	controlador de memoria, existe la posibilidad de que en las implementaciones 
	de ASM la cache tenga guardada ya toda la información que precisa el código 
	al repetir el experimento más de una vez.
	
	En cambio, al compilar, las implementaciones en C pueden traducirse en una
	mayor cantidad de operaciones de código ASM, por lo que es más factible
	que la cache vaya perdiendo información al ocuparse de estas instrucciones
	extra.

	Es probable que esto no pueda ser del todo apreciado en el segundo
	filtro ya que no solo se requieren realizar mas cálculos previos al ciclo que
	procesa la imagen, también se hace una llamada a una función en C que aumenta
	las probabilidades de que la cache se llene y se requiera liberar espacio,
	potencialmente perdiendo información guardada de la imagen a procesar.	

\subsection{Filtro Diff}

\subsubsection{Hipótesis}

	Nuestra Hipótesis original hablaba de un rendimiento 4 veces mejor del ASM
	sobre el C ya que a primera vista el código en ASM procesaba 4 píxeles en
	una iteración de ciclo mientras que el C procesaba solo 1.

	Durante la implementación encontramos que el ciclo principal de la
	implementación de C no procesa una píxel completo, requiere de cálculos
	individuales para cada componente (excluyendo la componente $a$ que se fija
	en ambos códigos por igual). Esto nos lleva a revisar nuestra hipótesis y
	plantear una nueva un poco mas fuerte que establece una mejora de
	performance del código ASM de al menos 12 veces.

	Llegamos a este número al comparar las implementaciones y ver que en cada
	conjunto de instrucciones de C para realizar el cálculo de la máxima
	diferencia para las 3 componentes de 1 píxel, en la implementación de ASM
	se computan 16 componentes en paralelo, si tenemos en cuenta que en ambos
	casos la componente $a$ se fija sin comparaciones, notamos que el código C
	con 12 (el cálculo de la diferencia absoluta de 3 componentes) operaciones
	procesa lo mismo que ASM en 1 (el cálculo de la diferencia absoluta de 16
	componentes).

	Otro punto a tener en cuenta es que la implementación en C hace un fetch a
	memoria por componente mientras que ASM en un solo fetch trae 4 píxeles (16
	componentes). De la misma forma para guardar el resultado, en C
	realizaremos 4 accesos a memoria (3 para guardar los datos calculados y 1
	para fijar la componente $a$ en la imagen de salida) mientras que en ASM
	con una instrucción se setea el $a$ de cuatro píxeles y con un acceso a
	memoria se guardan en la imagen de salida los 4 píxeles procesados. 

	Es por esto que esperamos que la performance de C/ASM sea al menos 12,
	podemos esperar mejores resultados con respecto a Assembler por las
	diferencias de accesos a memoria (con la intervención de la cache) y porque
	no se sabe a ciencia cierta como el compilador traducirá las instrucciones
	de C a código máquina.

\subsubsection{Planteo de experimentos}

	Para analizar nuestra hipótesis planteamos un experimento variando
	automáticamente el tamaño de dos imágenes, una completamente roja y otra
	completamente azul.
	Elegimos dos imágenes arbitrarias ya que las operaciones que se realizan
	tanto en C como en ASM no dependen del contenido de la imagen en si a la
	hora de la medición de tiempos.
	
	Comenzaremos con imágenes de 4x4 pixeles hasta 4096x4096 pixeles (4K) para
	obtener una amplia gama de resultados.
	
	Esperamos que que la relación tiempo vs ancho (igual a alto) sea cuadrática
	ya que al variar el ancho aumentamos de forma cuadrática la cantidad de
	pixeles de la imagen y el cálculo de la diferencia es lineal respecto de la
	cantidad de píxeles de la imagen (cada píxel se procesa una sola vez).

	En la presentación gráfica de los datos dividimos los tiempos por el ancho
	de la imagen logrando con esto linealizar el gráfico, ya que esperamos que
	sea cuadrática la función representada.

\subsubsection{Resultados}

	En el siguiente gráfico podemos ver la relación de Performance entre C y ASM:

	\begin{figure}[H]
	  \hspace{-2em}
		\begin{minipage}[h]{0.40\textwidth}
		\begin{center}
		\includegraphics[scale=0.30]{./imagenes/bench_diff_1_tiempo.png}
		\caption{Gráfico linealizado del tiempo C vs ASM a medida que aumenta el tamaño de las imágenes}
		\label{fig:ecm_test3}
		\end{center}
		\end{minipage}
		\hspace{5em}
		\begin{minipage}[h]{0.40\textwidth}
		\begin{center}
		\includegraphics[scale=0.30]{./imagenes/rel_bench_diff_1_tiempo.png}
		\caption{Performance ASM vs C a medida que aumenta el tamaño de la imagen}
		\label{fig:psnr_test3}
		\end{center}
		\end{minipage}
	\end{figure}

	Con estos resultados confirmamos que nuestra hipótesis inicial de que
	el rendimiento de ASM sería superior a 4 no era lo suficientemente fuerte, pero
	vemos que la hipótesis que formulamos mientras desarrollábamos el filtro de que
	sería superior a 12 era mucho más acertada.

	También observamos que en imágenes muy pequeñas la Performance de ASM
	es más de 20 veces superior a la de C. Esto lo atribuimos al trabajo de
	la cache y del controlador de memoria que mencionamos previamente. Por otro
	lado analizamos la conversión de C a ASM, utilizando el comando objdump sobre
	los archivos .o del filtro en C, y observamos que existe una gran diferencia en
	la cantidad de instrucciones que se deben ejecutar comparado con el código ASM.
	Esto, además de costo de procesamiento extra, puede producir una mayor cantidad
	de misses en cache cuando se procede a procesar la imagen como
	mencionamos previamente.

	A partir de los 700x700 píxeles vemos una estabilización de rendimiento
	alrededor del 15, número cercano al esperado por nuestra hipótesis. Al ser
	mayor la imagen, la cache toma un rol menos significativo en la relación, y
	deducimos que la diferencia de performance se debe ahora si principalmente a lo
	mencionado en nuestra hipótesis acerca del procesamiento con SIMD.

	Finalmente, para tener un mejor panorama de como rinde cada filtro, presentamos
	los datos calculados por separado (Figura 6), en él se puede ver el
	rendimiento linealizado según la implementación.

\subsection{Desenfoque Gaussiano}


\subsubsection{Hipotesis}

	Nuevamente nuestra hipótesis original planteaba un rendimiento 4 veces
	mejor de la implementación de ASM sobre la de C ya que el modelo de
	procesamiento SIMD permite trabajar en paralelo con paquetes de datos 4 veces
	mas grandes que en C.

	Sin embargo esperamos que el rendimiento sea mayor ya que, como
	mencionamos previamente, ambas implementación deben convertir los datos en
	representaciones de punto flotante y al final nuevamente a enteros antes de ser
	guardados en la imagen de destino y nuevamente SIMD tiene instrucciones
	para realizar estas transformaciones en paralelo para todos los datos
	procesados.

	Este requerimiento que nos hace trabajar con la precisión de los puntos
	flotantes es lo que nos hace pensar que aunque nuestra hipótesis original era
	muy débil, no vamos a conseguir tan buenos resultados como los
	obtenidos al evaluar el filtro Diff, ya que Diff puede procesar 4 pixeles a la
	vez mientras que Blur solo puede procesar un pixel ya que las 4 componentes en
	representación de punto flotante ocupan todo el espacio disponible en un
	registro $XMM$.

	Por otro lado, la implementación en C debe realizar 2 conversiones por
	cada componente procesada mientras que la implementación en ASM realizará solo
	2 conversiones cada 3 componentes procesadas, C también deberá realizar
	un acceso a memoria por cada componente $a$ para fijarla en 255 mientras que ASM
	fija esa componente y aprovecha en un acceso a memoria para guardar
	todas las componentes del pixel con una instrucción.

	Es por estos motivos que esperamos una performance de C/ASM mayor a 6
	pero menor a la obtenida con el filtro Diff.

\subsubsection{Planteo de experimentos}

	Realizaremos dos experimentos para evaluar nuestra hipótesis sobre este filtro.
	
	Revisaremos como afectan las mediciones de tiempo cuando se varía el
	radio y se fija el tamaño de la imagen a evaluar (utilizaremos la imagen Lena, Figura \ref{fig:img_blur}) y por otro lado fijaremos el radio (en particular $r=5$ para estos experimentos) mientras
	aumentaremos el tamaño de la imagen siguiendo el método descripto en la
	introducción de esta sección.

	El motivo por el cual no variamos $\sigma$ en ninguna experimentación
	se debe a que esta variable solo se utiliza para el cálculo de los valores de
	las celdas de la matriz K y en ningún momento se fuerza al algoritmo a realizar
	más o menos operaciones.

	Esperamos que ambos experimentos tengan resultados similares ya que en
	ambos casos lo que le vamos a estar exigiendo al algoritmo es procesar una
	mayor cantidad de operaciones, por cada pixel de la imagen a evaluar se
	debe recorrer toda la matriz K.
 
	Sin embargo, cuando variamos el tamaño de la imagen y dejamos fijo el
	radio vamos a poder evaluar cuanto se demoran los fetch a memoria y cuanto
	influye la caché, la cantidad de operaciones del ciclo principal quedarán
	fijas debido a que el tamaño de la matriz K queda fija. Por otro lado, cuando
	aumentamos el radio, el algoritmo va a realizar más cálculos por cada
	fetch a memoria ya que el tamaño de la matriz K será más grande y ahí podremos
	ver cómo el uso del modelo SIMD puede reducir el tiempo de ejecución
	total de la implementación en ASM.

\subsubsection{Resultados}

	\begin{figure}[H]
	  \hspace{-2em}
		\begin{minipage}[h]{0.40\textwidth}
		\begin{center}
		\includegraphics[scale=0.30]{./imagenes/bench_blur_tvar_1_tiempo.png}
		\caption{Gráfico linealizado del tiempo C vs ASM a medida que aumenta el tamaño de las imágenes}
		\label{fig:blur_tvar}
		\end{center}
		\end{minipage}
		\hspace{5em}
		\begin{minipage}[h]{0.40\textwidth}
		\begin{center}
		\includegraphics[scale=0.30]{./imagenes/rel_bench_blur_tvar_1_tiempo.png}
		\caption{Performance ASM vs C a medida que aumenta el tamaño de la imagen}
		\label{fig:rel_blur_tvar}
		\end{center}
		\end{minipage}
	\end{figure}
	
	Como vemos en la Figura \ref{fig:blur_tvar}, los resultados para tamaños de
	imágenes menores a 300x300 pixeles son muy similares entre las dos
	implementaciones, y esto se lo atribuimos al trabajo de la caché. Como lo
	habíamos anticipado en este tipo de imágenes la caché va a ayudar a reducir los
	accesos a memoria y, como explicamos en el Planteo de Experimentos, los
	resultados de este experimento al tener fijo el radio van a depender mucho de la
	demora de los fetch de la imagen a procesar.

	Cuando miramos los resultados obtenidos con imágenes mayores a 300x300 pixeles
	notamos una pendiente de crecimiento más pronunciada para la implementación en
	C. Evaluando estos resultados junto al crecimiento de la implementación en ASM
	notamos que esta diferencia se debe principalmente a qué ASM puede procesar un
	pixel entero por cada acceso a memoria mientras que C puede realizar solo una
	componente por vez.

	En la Figura \ref{fig:rel_blur_tvar} vemos como en imágenes de tamaños menores la
	performance varía mucho pero siempre en valores mayores a 3,96. Este
	mínimo coincide con nuestra hipótesis original, y estimamos que para imágenes de
	192x192 pixeles ambas implementaciones tienen un similar aprovechamiento de la
	memoria caché y la diferencia de tiempos se da por la cantidad de datos
	procesados en paralelos por el modelo SIMD.

	A medida que van creciendo en tamaño las imágenes vemos como la relación C/ASM
	va acercándose a nuestra segunda hipótesis y se consiguen resultados mayores a
	6 a partir de imágenes de tamaño 384x384 pixeles.

	\begin{figure}[H]
	  \hspace{-2em}
		\begin{minipage}[h]{0.40\textwidth}
		\begin{center}
		\includegraphics[scale=0.30]{./imagenes/bench_blur_rvar_1_tiempo.png}
		\caption{Gráfico linealizado del tiempo C vs ASM a medida que aumenta el radio de la matriz $K$}
		\label{fig:blur_rvar}
		\end{center}
		\end{minipage}
		\hspace{5em}
		\begin{minipage}[h]{0.40\textwidth}
		\begin{center}
		\includegraphics[scale=0.30]{./imagenes/rel_bench_blur_rvar_1_tiempo.png}
		\caption{Performance ASM vs C a medida que aumenta el radio de la matriz $K$}
		\label{fig:rel_blur_rvar}
		\end{center}
		\end{minipage}
	\end{figure}

		Este experimento esperamos que muestre mejor como se aprovecha el
	modelo SIMD ya que, según lo que indicamos en el Planteo del Experimento,
	estaremos trabajando con una imagen de tamaño fijo pero aumentando el radio de
	la matriz K.
	
	Como vemos en la Figura \ref{fig:blur_rvar} la pendiente de la
	implementación C no solo es mayor que la de ASM sino que, a diferencia del
	experimento anterior, no tiene grandes variaciones. De la misma forma quedaron
	los resultados de la implementación en ASM, tiene una pendiente mucho mas
	pequeña y sin grandes variaciones.

	Esto se ve en la Figura \ref{fig:rel_blur_rvar}, la relación C/ASM aumenta a
	medida que aumenta el radio acercándose a valores cercanos a 7 para radios
	mayores a 6. 

	En el primer valor de radio 1, que genera una matriz K de 3x3, vemos una
	performance de 3,75 bastante cercana a nuestra hipótesis original, y a medida
	que la matriz K aumenta el algoritmo puede aprovechar más el procesamiento en
	paralelo de los datos y conseguir mejores valores C/ASM.
	 
