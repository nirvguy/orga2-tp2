#!/bin/bash
ARCHIVO_STAT=$1
SALIDA_STAT=$2

temp_1=$(mktemp /tmp/merge_XXXXXXXX)
temp_2=$(mktemp /tmp/merge_XXXXXXXX)

grep ^0 $ARCHIVO_STAT > $temp_1
grep ^1 $ARCHIVO_STAT > $temp_2

paste -d' ' $temp_1 $temp_2 > $SALIDA_STAT

rm $temp_1 $temp_2
