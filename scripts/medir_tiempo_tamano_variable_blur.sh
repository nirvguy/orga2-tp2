#!/bin/bash
# Uso: 
#   ./medir_tiempo_radio_variable_blur.sh IMAGEN SIGMA RADIO TAM_BEGIN TAM_STEP TAM_END IMPL ITER
# donde
#		IMAGEN = Imagen
#		RADIO = Sigma
#		TAM_BEGIN = Desde que radio
#		TAM_STEP = Salto de radio (ej: de a 1, de a 2)
#		TAM_END = Hasta que radio
#		IMPL	  = 0 (c) / 1 (asm)
#		ITER	  = cantidad de iteraciones
IMAGEN=$1
SIGMA=$2
RADIO=$3
TAM_BEGIN=$4
TAM_STEP=$5
TAM_END=$6
IMPL=$7
if [ $IMPL == '0' ] ; then
	IMPL_S='c'
else
	IMPL_S='asm'
fi

if test $# -lt 8; then 
	TIMES=1
else
	TIMES=$8
fi

IMG_TEMP=$(mktemp /tmp/blur_XXXXXXXX.bmp)

cp $IMAGEN $IMG_TEMP

for tam in $(seq $TAM_BEGIN $TAM_STEP $TAM_END)
do
	convert $IMG_TEMP -resize $(echo $tam)x$(echo $tam)! $IMG_TEMP
	for t in $(seq 1 $TIMES)
	do
		echo -n "$IMPL $tam"
		../build/tp2 -i $IMPL_S blur $IMG_TEMP $SIGMA $RADIO | tail -n 1 | cut -d':' -f2
	done
done

rm $IMG_TEMP
