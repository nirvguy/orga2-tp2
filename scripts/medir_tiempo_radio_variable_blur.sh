#!/bin/bash
# Uso: 
#   ./medir_tiempo_radio_variable_blur.sh IMAGEN SIGMA RADIO_BEGIN RADIO_STEP RADIO_END IMPL ITER
# donde
#		IMAGEN = Imagen a blurear
#		SIGMA  = Sigma
#		RADIO_BEGIN = Desde que radio
#		RADIO_STEP = Salto de radio (ej: de a 1, de a 2)
#		RADIO_END = Hasta que radio
#		IMPL	  = 0 (c) / 1 (asm)
#		ITER	  = cantidad de iteraciones
IMAGEN=$1
SIGMA=$2
RADIO_BEGIN=$3
RADIO_STEP=$4
RADIO_END=$5
IMPL=$6
if [ $IMPL == '0' ] ; then
	IMPL_S='c'
else
	IMPL_S='asm'
fi

if test $# -lt 7; then 
	TIMES=1
else
	TIMES=$7
fi

for R in $(seq $RADIO_BEGIN $RADIO_STEP $RADIO_END)
do
	for t in $(seq 1 $TIMES)
	do
		echo -n "$IMPL $R"
		../build/tp2 -i $IMPL_S blur $IMAGEN $SIGMA $R | tail -n 1 | cut -d':' -f2
	done
done
