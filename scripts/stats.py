#!/usr/bin/env python3
# encoding: utf-8
import os
import statistics
import sys

def parse_line(line):
    return [i for i in line.strip().split(' ')]

def print_stats(impl,r,times):
    mean = statistics.mean(times)
    stdev = statistics.stdev(times)
    print("%u %u %f %f" % (impl, r, mean, stdev))


def main():
    data = [{},{}]
    for line in sys.stdin:
        str_imp,str_r,str_time = parse_line(line)
        imp = int(str_imp)
        r = int(str_r)
        time = float(str_time)
        if not r in data[imp]:
            data[imp][r] = [[]]
        data[imp][r][0].append(time)
    for i in range(0,2):
        for r,l in data[i].items():
            print_stats(i,r,l[0])

if __name__ == "__main__":
    main()
