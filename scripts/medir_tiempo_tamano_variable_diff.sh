#!/bin/bash
# Uso: 
#   ./medir_tiempo_radio_variable_blur.sh IMAGEN SIGMA TAM_BEGIN TAM_STEP TAM_END IMPL ITER
# donde
#		IMAGEN_1 = Imagen 1
#		IMAGEN_2 = Imagen 2
#		TAM_BEGIN = Desde que radio
#		TAM_STEP = Salto de radio (ej: de a 1, de a 2)
#		TAM_END = Hasta que radio
#		IMPL	  = 0 (c) / 1 (asm)
#		ITER	  = cantidad de iteraciones
IMAGEN_1=$1
IMAGEN_2=$2
TAM_BEGIN=$3
TAM_STEP=$4
TAM_END=$5
IMPL=$6
if [ $IMPL == '0' ] ; then
	IMPL_S='c'
else
	IMPL_S='asm'
fi

if test $# -lt 7; then 
	TIMES=1
else
	TIMES=$7
fi

IMG1_TEMP=$(mktemp /tmp/diff_0_XXXXXXXX.bmp)
IMG2_TEMP=$(mktemp /tmp/diff_1_XXXXXXXX.bmp)

cp $IMAGEN_1 $IMG1_TEMP
cp $IMAGEN_2 $IMG2_TEMP

for T in $(seq $TAM_BEGIN $TAM_STEP $TAM_END)
do
	convert $IMG1_TEMP -resize $(echo $T)x$(echo $T)! $IMG1_TEMP
	convert $IMG2_TEMP -resize $(echo $T)x$(echo $T)! $IMG2_TEMP
	for t in $(seq 1 $TIMES)
	do
		echo -n "$IMPL $T"
		../build/tp2 -i $IMPL_S diff $IMG1_TEMP $IMG2_TEMP | tail -n 1 | cut -d':' -f2
	done
done

rm $IMG1_TEMP
rm $IMG2_TEMP
