default rel
global _blur_asm
global blur_asm
extern generar_matriz
extern liberar_matriz


section .rodata
	zero: dd 0.0, 0.0, 0.0, 0.0
	mask_extender_pixel: db 0x0, 0x80, 0x80, 0x80,\
	                        0x1, 0x80, 0x80, 0x80,\
	                        0x2, 0x80, 0x80, 0x80,\
	                        0x3, 0x80, 0x80, 0x80

	mask_comprimir_pixel: db 0x0, 0x4, 0x8, 0xC, \
							0x0, 0x0, 0x0, 0x0,\
	                        0x0, 0x0, 0x0, 0x0,\
	                        0x0, 0x0, 0x0, 0x0
	TRUE equ  0x1
	FALSE equ 0x0
section .data
	



section .text


generarMatriz:
	push rdi
	push rsi
	push rdx
	push rcx
	push r8
	mov rdi, r8
	call generar_matriz
	pop r8
	pop rcx
	pop rdx
	pop rsi
	pop rdi
	ret


calcularFinImagen:
	;r12 (direccion en el fin de la vector imagen) <--- filas*columnas*sizeof(pixel)
	;mov r12, rdi+rdx*rcx*4
	push rax
	push rdx
	push rcx
	mov eax, ecx
	mul edx
	xor rcx, rcx
	mov ecx, eax
	shl rdx, 32
	or rcx, rdx
	shl rcx, 2
	mov r12, rcx
	add r12, rdi
	pop rcx
	pop rdx
	pop rax
	ret
	
esBorde:
	;actualiza filas y columnas
	;si n°col == col => col = 0, fila = fila+1
	cmp [rbp-8], rcx
	jne .checkBorde
	.nuevaCol:
		mov qword [rbp-8], 0
		inc qword [rbp-16]
		
	.checkBorde:
	;si n°col > r && n°col < cols-r && n°fila > r && n°fila < filas-r calcularBlur
	;si no poner la imagen tal cual
	cmp [rbp-8], r8
	jl .esBorde
	cmp [rbp-16], r8
	jl .esBorde
	mov r9, [rbp-8]
	add r9, r8
	cmp r9, rcx
	jge .esBorde
	mov r9, [rbp-16]
	add r9, r8
	cmp r9, rdx
	jge .esBorde
	.noEsBorde:
	mov r9, FALSE
	jmp .fin
	.esBorde:
	mov r9, TRUE
	.fin:
	ret

desplazarHaciaAbajo:
	;calcula r14 = r14+cols*4-(2*r+1)*4
	mov r13, rcx
	shl r13, 2
	add r14, r13
	mov r13, r8
	shl r13, 1
	inc r13
	shl r13, 2
	sub r14, r13
	ret
	
desplazarAPrimerVecino:
	;en r14 deja puntero al primer pixel de la imagen 
	;tomando como referencia la matriz K
	;r14= r14-rcx*r8*4-r8*4
	push rax
	push rdx
	push rcx
	mov eax, ecx
	mul r8d
	xor rcx, rcx
	mov ecx, eax
	shl rdx, 32
	or rcx, rdx
	shl rcx, 2
	sub r14, rcx
	mov rcx, r8
	shl rcx, 2
	sub r14, rcx
	pop rcx
	pop rdx
	pop rax
	ret


;void blur_asm    (
	;unsigned char *src
	;unsigned char *dst
	;int filas
	;int cols
    ;float sigma
    ;int radius)
	
	;src    =    rdi
	;dst    =    rsi
	;filas  =    edx
	;cols   =    ecx
    ;sigma  =    xmm0
    ;radius =    r8
	;rax    =    direccion de memoria donde comienza K
	;r14    =    direccion de vecino de la imagen (src) que estoy recorriendo
	;r15    =    direccion en donde termina la columna que voy recorriendo en K
	;r12	=    direccion donde termina la imagen src
	;r11    =    direccion de columna que estoy recorriendo en la matriz K
	;r10    =    direccion de fila que estoy recorriendo en la matriz K
	;rbx    =    direccion donde termina K
	;xmm0   =    resultado parcial
	;xmm1   =    pixel de src (en float (argb))
	;xmm2   =    valor de la celda que voy recorriendo en K
	;[rbp-16]  =    n° fila
	;[rbp-8]  =  n° col
_blur_asm:
blur_asm:
	push rbp
	mov rbp, rsp
	sub rsp, 16
	push rbx
	push r12
	push r13
	push r14
	push r15
	
	call generarMatriz
	

	;limpiamos la parte alta de filas, cols y radio (32 bits)
	mov r9d, r9d
	mov ecx, ecx
	mov edx, edx
	
	;en r12 nos deja la direccion donde esta el final de la imagen
	call calcularFinImagen

	
	;comienza contador de filas y cols en 0
	mov qword [rbp-16], 0
	mov qword [rbp-8], 0
	
	.recorrerImagen:
		;r10 empieza apuntando a la primer fila de la matriz K
		mov r10, rax
		;rbx apunta a la ultima fila de la matriz K
		mov rbx, r8
		shl rbx, 4
		add rbx, 8
		add rbx, r10

		mov r14, rdi
		
		.checkBorde:
		call esBorde
		cmp r9, TRUE
		jne .calcularBlur
		movdqu xmm0, [r14]
		jmp .copiarResultado
		
		.calcularBlur:
		;r14 se va r filas para arriba y r filas para la izquierda
		call desplazarAPrimerVecino
		
		movdqu xmm0, [zero]
		.recorrerKy:
			mov r11, [r10]
			;r15 apunta adonte a la dirección donde termina la fila de K que se esta recorriendo
			;r15 = r11+(2*r+1)*sizeof(float)=r11+2*r*sizeof(float)+sizeof(float)=r11+r8*8+4
			lea r15, [r11+r8*8+4]
			
			.recorrerKx:
				;traer pixel de src
				movdqu xmm1, [r14]
				pshufb xmm1, [mask_extender_pixel]
				cvtdq2ps xmm1, xmm1
				
				;trae celda de k y la repite
				movd xmm2, [r11]
				pshufd xmm2, xmm2, 0
				
				;calcula promedio parcial
				mulps xmm1, xmm2
				addps xmm0, xmm1
				
				add r14, 4
				add r11, 4
				cmp r11, r15
				jne .recorrerKx
			.finRecorrerKx:
			
			;desplaza r14 hacia abajo
			call desplazarHaciaAbajo
			
			;mueve r10 a la siguiente fila
			add r10, 8
			cmp r10, rbx
			jne .recorrerKy
		.finRecorrerKy:
		
		;guarda en valor pixel resultado (solo 32 bits)
		cvtps2dq xmm0, xmm0
		pshufb xmm0, [mask_comprimir_pixel]
		.copiarResultado:
		pextrd [rsi], xmm0, 0

		;mueve rdi al siguiente pixel
		add rdi, 4
		;mueve rsi al siguiente pixel
		add rsi, 4
		inc qword [rbp-8]
		cmp rdi, r12
		jne .recorrerImagen
	mov rdi, rax
	mov rsi, r8
	call liberar_matriz
	
	pop r15
	pop r14
	pop r13
	pop r12
	pop rbx
	add rsp, 16
	pop rbp
    ret
