
#include <stdlib.h>
#include <math.h>
#include "../tp2.h"

#define DIST(X,Y)      ((X >= Y) ? (X-Y) : (Y-X))

void diff_c (
	unsigned char *src,
	unsigned char *src_2,
	unsigned char *dst,
	int m,
	int n,
	int src_row_size,
	int src_2_row_size,
	int dst_row_size
) {
	unsigned char (*src_matrix)[src_row_size] = (unsigned char (*)[src_row_size]) src;
	unsigned char (*src_2_matrix)[src_2_row_size] = (unsigned char (*)[src_2_row_size]) src_2;
	unsigned char (*dst_matrix)[dst_row_size] = (unsigned char (*)[dst_row_size]) dst;
	
	int i,j,k=0;
	for(i=0;i<n;i++) {
		for(j=0;j<(m << 2);j+=4) {
			unsigned char max_comp = DIST(src_matrix[i][j],src_2_matrix[i][j]);
			for(k=1;k<3;k++) {
				unsigned char comp_1 = src_matrix[i][j+k];
				unsigned char comp_2 = src_2_matrix[i][j+k];
				unsigned char comp_diff = DIST(comp_1,comp_2);
				if(max_comp < comp_diff)
					max_comp = comp_diff;
			}
			dst_matrix[i][j] = max_comp;
			dst_matrix[i][j+1] = max_comp;
			dst_matrix[i][j+2] = max_comp;
			dst_matrix[i][j+3] = 0xFF;
		}
	}
}
