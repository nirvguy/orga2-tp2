default rel
global _diff_asm
global diff_asm

section .rodata
	ALPHA_TO_255: dd 0xFF000000, 0xFF000000, 0xFF000000, 0xFF000000
	PRESERVE_MAX: db 0x02, 0x02, 0x02, 0x02, \
					 0x06, 0x06, 0x06, 0x06, \
					 0x0A, 0x0A, 0x0A, 0x0A, \
					 0x0E, 0x0E, 0x0E, 0x0E

section .data

section .text

_diff_asm:
diff_asm:
;void diff_asm    (
	;unsigned char *src,
    ;unsigned char *src2,
	;unsigned char *dst,
	;int filas,
	;int cols)
	push rbp
	mov rbp, rsp

	;rcx=filas*columnas/4
	push rdx
	mov eax, ecx
	mul r8d
	xor rcx, rcx
	mov ecx, eax
	shl rdx, 32
	or rcx, rdx
	shr rcx, 2
	pop rdx
	
	.ciclo:
		;traemos los dats
		movdqu xmm0, [rdi]
		movdqu xmm1, [rsi]
		movdqu xmm2, xmm0
		
		;calculamos la diferencia en valor absoluto de las componentes
		pmaxub xmm0, xmm1
		pminub xmm2, xmm1
		psubb xmm0, xmm2


		;maximo
		movdqu xmm1, xmm0
		movdqu xmm2, xmm0
		pslldq xmm1, 1
		pslldq xmm2, 2

		pmaxub xmm0, xmm1
		pmaxub xmm0, xmm2
		pshufb xmm0, [PRESERVE_MAX]
		por xmm0, [ALPHA_TO_255]

		;guardamos el resultado
		movdqu [rdx], xmm0

		add rdi, 16
		add rsi, 16
		add rdx, 16
		loop .ciclo
	pop rbp

    ret
