#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "../tp2.h"

#define MAX(A,B)    ((A>=B) ? A : B)
#define MIN(A,B)    ((A<=B) ? A : B)
#define PI2         6.28318530718


float** generar_matriz(float sigma, int radius) {
	int TK=2*radius+1;
	float **K = (float**) malloc((TK)*sizeof(float*));
	int i=0;
	for(;i<(TK);i++)
		K[i] = (float*) malloc(2*(radius+1)*sizeof(float));
	float sigma_cuad = sigma*sigma;
	int x,y;
	for(x=0;x<=(2*radius);x++)
		for(y=0;y<=(2*radius);y++)
			K[x][y] = exp(-((radius-x)*(radius-x)+(radius-y)*(radius-y))/(2.0*sigma_cuad))/(PI2*sigma_cuad);
	return K;
}


void liberar_matriz(float **K, int radius) {
	int TK=2*radius+1;
	int i=0;
	for(;i<(TK);i++)
		free(K[i]);
	free(K);
}

void blur_c    (
    unsigned char *src,
    unsigned char *dst,
    int cols,
    int filas,
    float sigma,
    int radius)
{
    unsigned char (*src_matrix)[cols*4] = (unsigned char (*)[cols*4]) src;
    unsigned char (*dst_matrix)[cols*4] = (unsigned char (*)[cols*4]) dst;
	/* float **K[TK][TK] */
	int i=0;
	float **K;
	K = generar_matriz(sigma, radius);
	i=0;
	int j=0;
	for(i=radius;i<filas-radius;i++) {
		for(j=radius*4;j<(cols-radius)*4;j+=4) {
			int rx,ry;
			float res_r = 0.0;
			float res_g = 0.0;
			float res_b = 0.0;
			for(ry=-radius;ry<=radius;ry++) {
				for(rx=-radius;rx<=radius;rx++) {
					float b=(float) src_matrix[ry+i][rx*4+j];
					float g=(float) src_matrix[ry+i][rx*4+j+1];
					float r=(float) src_matrix[ry+i][rx*4+j+2];
					float k=K[ry+radius][rx+radius];
					res_b += b * k;
					res_g += g * k;
					res_r += r * k;
				}
			}
			dst_matrix[i][j]   = (char) res_b;
			dst_matrix[i][j+1] = (char) res_g;
			dst_matrix[i][j+2] = (char) res_r;
			dst_matrix[i][j+3] = 0xFF;
		}
	}
	liberar_matriz(K,radius);
}
