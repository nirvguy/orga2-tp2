MEDIR_TIEMPOS_BLUR_R_VARIABLE=../scripts/medir_tiempo_radio_variable_blur.sh
MEDIR_TIEMPOS_BLUR_T_VARIABLE=../scripts/medir_tiempo_tamano_variable_blur.sh
MEDIR_TIEMPOS_DIFF_T_VARIABLE=../scripts/medir_tiempo_tamano_variable_diff.sh
MEDIR_TIEMPO_REL=../scripts/tiempo_rel.sh
STATS=../scripts/stats.py

R_BEGIN=1
R_STEP=1
R_END=10

RADIO=4

TAM_DIFF_BGIN=4
TAM_DIFF_STEP=64
TAM_DIFF_END=4096

TAM_BLUR_BEGIN=64
TAM_BLUR_STEP=64
TAM_BLUR_END=640

ITER=20
SIGMA=5

IMG_TEST=../test
# DATA_IMG=bench_blur_1.bmp bench_diff_1_1.bmp
DATA_TIEMPO=bench_blur_rvar_1.dat bench_blur_tvar_1.dat bench_diff_1.dat
DATA_STATS=bench_blur_rvar_1.stat bench_blur_tvar_1.stat bench_diff_1.stat
DATA_REL=bench_blur_rvar_1.rel bench_blur_tvar_1.rel bench_diff_1.rel
DATA_PLOTS=bench_blur_rvar_1_tiempo.png bench_blur_tvar_1_tiempo.png bench_diff_1_tiempo.png
PLOTS_REL=rel_bench_blur_rvar_1_tiempo.png rel_bench_blur_tvar_1_tiempo.png rel_bench_diff_1_tiempo.png

all: $(DATA_STATS) $(DATA_TIEMPO) $(DATA_PLOTS) $(PLOTS_REL) $(DATA_REL)

bench_%.stat: bench_%.dat
	$(STATS) < $< > $@; sort -snk2 $@ | sort -snk1 --output=$@

bench_diff_%.dat: $(IMG_TEST)/bench_diff_%_1.bmp
	$(MEDIR_TIEMPOS_DIFF_T_VARIABLE) $(patsubst %_1.bmp,%_0.bmp,$<) $< $(TAM_DIFF_BGIN) $(TAM_DIFF_STEP) $(TAM_DIFF_END) 0 $(ITER) > $@
	$(MEDIR_TIEMPOS_DIFF_T_VARIABLE) $(patsubst %_1.bmp,%_0.bmp,$<) $< $(TAM_DIFF_BGIN) $(TAM_DIFF_STEP) $(TAM_DIFF_END) 1 $(ITER) >> $@
	
bench_blur_tvar_%.dat: $(IMG_TEST)/bench_blur_tvar_%.bmp
	$(MEDIR_TIEMPOS_BLUR_T_VARIABLE) $< $(SIGMA) $(RADIO) $(TAM_BLUR_BEGIN) $(TAM_BLUR_STEP) $(TAM_BLUR_END) 0 $(ITER) > $@
	$(MEDIR_TIEMPOS_BLUR_T_VARIABLE) $< $(SIGMA) $(RADIO) $(TAM_BLUR_BEGIN) $(TAM_BLUR_STEP) $(TAM_BLUR_END) 1 $(ITER) >> $@

bench_blur_rvar_%.dat: $(IMG_TEST)/bench_blur_rvar_%.bmp
	$(MEDIR_TIEMPOS_BLUR_R_VARIABLE) $< $(SIGMA) $(R_BEGIN) $(R_STEP) $(R_END) 0 $(ITER) > $@
	$(MEDIR_TIEMPOS_BLUR_R_VARIABLE) $< $(SIGMA) $(R_BEGIN) $(R_STEP) $(R_END) 1 $(ITER) >> $@

bench_blur_rvar_%_tiempo.png: bench_blur_rvar_%.stat
	gnuplot -e "archivo_data='$<';archivo_salida='$@';etiqueta_x='Radio'" plot_radio.gpi

bench_blur_tvar_%_tiempo.png: bench_blur_tvar_%.stat
	gnuplot -e "archivo_data='$<';archivo_salida='$@';etiqueta_x='Tamaño en pixeles'" plot_radio.gpi

bench_diff_%_tiempo.png: bench_diff_%.stat
	gnuplot -e "archivo_data='$<';archivo_salida='$@';etiqueta_x='Tamaño en pixeles'" plot_radio.gpi

rel_bench_blur_rvar_%_tiempo.png: bench_blur_rvar_%.rel
	gnuplot -e "archivo_data='$<';archivo_salida='$@';etiqueta_x='Radio'" plot_rel.gpi

rel_bench_blur_tvar_%_tiempo.png: bench_blur_tvar_%.rel
	gnuplot -e "archivo_data='$<';archivo_salida='$@';etiqueta_x='Tamaño en pixeles'" plot_rel.gpi

rel_bench_diff_%_tiempo.png: bench_diff_%.rel
	gnuplot -e "archivo_data='$<';archivo_salida='$@';etiqueta_x='Tamaño en pixeles'" plot_rel.gpi

bench_%.rel: bench_%.stat
	$(MEDIR_TIEMPO_REL) $< $@

	
clean:
	rm -rf $(DATA_TIEMPO) $(DATA_PLOTS) $(DATA_STATS) $(DATA_REL) $(PLOTS_REL)

.PHONY: clean

